
// require('./bootstrap');
import Vue from 'vue'
import MainApp from './views/MainApp.vue'
import {routes} from './routes'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import StoreData from './store'
import {initialize} from './helpers/general'

// vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'


//Swiper

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

//AOS

import AOS from 'aos'
import 'aos/dist/aos.css';

//VeeValidate

import VeeValidate from 'vee-validate';

Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Vuetify , {
    iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4'
})
Vue.use(VueAwesomeSwiper, /* { default global options } */)
Vue.use(VeeValidate , {
  fieldsBagName: 'veeFields'
})

const store = new Vuex.Store(StoreData)

const router = new VueRouter({
    routes,
    mode: 'history'
})

initialize(store, router)

// window.Vue = require('vue');

const app = new Vue({
    el: '#app',
    router,
    store,
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdi',
        }
    }),
    components:{
        MainApp,
    },
    created() {
        AOS.init();
    }
});
