import Axios from "axios";
import router from "vue-router";

export function login(credentials) {
    return new Promise((res , rej) => {
        Axios.post('/api/auth/login' , credentials)
        .then((response) => {
            res(response.data);
            location.reload();
        })
        .catch((err) => {
            rej("Wrong email or password");
        })
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");
    console.log(userStr)
    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}