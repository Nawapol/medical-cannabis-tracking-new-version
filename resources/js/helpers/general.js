import Axios from "axios";

export function initialize(store, router) {

    // router.beforeEach((to, from, next) => {
    //     const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    //     const currentUser = store.state.currentUser;
    
    //     if(requiresAuth && !currentUser) {
    //         next('/login');
    //     } else if(to.path == '/login' && currentUser) {
    //         next('/');
    //     } else {
    //         next();
    //     }
    // });

    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentUser = store.state.currentUser;
        if(requiresAuth) {
            
            if(!currentUser || !currentUser.token) {
                next('/login');
            }else if(to.meta.superAdminAuth) {
                if(currentUser.type === 'super_admin' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.AdminAuth) {
                if(currentUser.type === 'farm_admin' || currentUser.type === 'transport_admin' || currentUser.type === 'factory_admin' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.farmAdminAuth) {
                if(currentUser.type === 'farm_admin' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.transportAdminAuth){
                if(currentUser.type === 'transport_admin' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.factoryAdminAuth) {
                if(currentUser.type === 'factory_admin' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.agencyAuth) {
                if(currentUser.type === 'agency' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.farmStaffAuth) {
                if(currentUser.type === 'farm_staff' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.transportStaffAuth) {
                if(currentUser.type === 'transport_staff' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }else if(to.meta.factoryStaffAuth) {
                if(currentUser.type === 'factory_staff' && currentUser.user_status !== 'ban') {
                    next();
                }else{
                    next('/');
                }
            }
            else {
                next('/');
            }
        } else if (requiresAuth && !currentUser) {
            next('/login');
        } else if(to.path == '/login' && currentUser) {
            next('/');
        } else {
            next();
        }
    })
    
    Axios.interceptors.response.use(null, (error) => {
        if (error.response.status = 401) {
            store.commit('logout');
            router.push('/login');
        }
    })

}