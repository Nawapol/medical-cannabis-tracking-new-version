import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import CheatSuperAdmin from './views/CheatSuperAdmin.vue'

export const routes = [
    {
        path: '/',
        component: Home,
        name: 'home',
    },
    {
        path: '/login',
        component: Login,
        name: 'login'
    },
    {
        path: '/register',
        component: Register,
        name: 'register'
    },
    {
        path: '/cheat-superadmin',
        component: CheatSuperAdmin,
        name: 'cheat-superadmin'
    }
]