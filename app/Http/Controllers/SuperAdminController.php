<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \stdClass;

class SuperAdminController extends Controller
{
    function cheatSuperAdmin()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('http://localhost:8080/cheatSuperAdmin');
        $data = $res->getBody()->getContents();
        $json_decode = json_decode($data);

        $password = bcrypt('123456');

        $sql_user = DB::table('users')->insert([
            ['type' => 'super_admin', 'key' => $json_decode->key, 'user_id' => 1, 'location_id' => 0, 'address' => $json_decode->address, 'user_status' => 'active',
            'email' => 'super_admin@gmail.com','password' => $password,'name' => 'SuperAdmin']
        ]);

        info($sql_user);
        return response()->json($sql_user);
    }
}
