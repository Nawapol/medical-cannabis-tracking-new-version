<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \stdClass;

class UserController extends Controller
{
    public function checkUser()
    {
        $email = request('email');
        $sql_user = DB::table('users')->select('users.email','users.name','users.surname','users.picture')->where('email', $email)->get();
        $encode = json_encode($sql_user);
        return response()->json($encode);
    }
}
