<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('ssn')->uniqid()->nullable();
            $table->string('sex')->nullable();
            $table->date('birtyday')->nullable();
            $table->string('address')->uniqid()->nullable();
            $table->string('type');
            $table->string('key')->uniqid();
            $table->string('user_status')->nullable();
            $table->string('license')->uniqid()->nullable();
            $table->string('drive_license')->uniqid()->nullable();
            $table->string('picture')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('location_id')->unsigned();
            // $table->foreign('location_id')->references('id')->on('locations');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}