<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTakeCareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('take_cares', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('plant_lot_id')->unsigned();
            $table->primary(['user_id','plant_lot_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('plant_lot_id')->references('id')->on('plant_lots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('take_cares');
    }
}
