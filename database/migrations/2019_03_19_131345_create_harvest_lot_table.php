<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHarvestLotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_lots', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('harvest_key')->uniqid();
            $table->integer('amount'); 
            $table->dateTime('harvest_date'); 
            $table->string('harvest_status'); 
            $table->integer('lot_id_plant')->unsigned();
            $table->foreign('lot_id_plant')->references('id')->on('plant_lots');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_lots');
    }
}
