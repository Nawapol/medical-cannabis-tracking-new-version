<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->increments('id'); 
            $table->string('transport_key')->uniqid();
            $table->integer('amount_export');
            $table->integer('amount_import')->nullable();
            $table->string('status');
            $table->dateTime('date_start');
            $table->dateTime('date_arrive')->nullable();
            $table->integer('cid_transport')->unsigned();
            $table->foreign('cid_transport')->references('id')->on('cars');
            $table->integer('location_id_import')->unsigned();
            $table->foreign('location_id_import')->references('id')->on('locations');
            $table->integer('hl_id_export')->unsigned();
            // $table->foreign('hl_id_export')->references('id')->on('harvest_lots');
            $table->integer('transport_location_id')->unsigned();
            $table->foreign('transport_location_id')->references('id')->on('locations');
            $table->integer('driver_id')->unsigned();
            $table->foreign('driver_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transports');
    }
}
