<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantLotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plant_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gene');
            $table->string('lot_address')->uniqid();
            $table->string('lot_key')->uniqid();
            $table->string('status_lot');
            $table->integer('total_tree');
            $table->integer('total_dead')->nullable();
            $table->integer('total_amount')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->boolean('status_destroy');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plant_lots');
    }
}
